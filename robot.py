#!/usr/bin/env python3
"""
    Robot is the main file for robot code. Here, objects are instantiated,
    and joystick interaction happens.
"""

import magicbot
from magicbot import tunable

import wpilib
import helpers

from components import climber
from components import drive
from components import dumper
from components import camera
from components import camera
from components import gear_catcher

from common import ir as IR

import leds as Leds

from robotpy_ext.control.button_debouncer import ButtonDebouncer

from networktables import NetworkTable

ir = IR.IR

leds = Leds.Leds()


class MyRobot(magicbot.MagicRobot):
    """ Controls what parts of the robot go when """

    # Automations first

    # Components last
    drive = drive.Drive
    climber = climber.Climber
    dumper = dumper.Dumper
    camera = camera.Camera
    gear_catcher = gear_catcher.GearCatcher

    def createObjects(self):
        """
            Create basic objects (motor controllers, joysticks, etc.)

            These objects can be used in other components easily
            For example, in /components/drive.py:

            robot_drive = wpilib.RobotDrive

            In the components, it isn't neccesary to specify parameters,
                just the type.
        """

        self.ds = wpilib.DriverStation.getInstance()

        # Motor Controllers

        self.left_climber_motor = wpilib.Talon(2)
        self.right_climber_motor = wpilib.Talon(3)

        self.dumper_motor = wpilib.Spark(4)

        self.left_drive_motor = wpilib.Spark(0)
        self.right_drive_motor = wpilib.Spark(1)

        self.robot_drive = wpilib.RobotDrive(
            self.left_drive_motor, self.right_drive_motor)

        # OI

        self.driver_stick = wpilib.Joystick(0)
        self.operator_stick = wpilib.Joystick(1)

        # Sensors

        self.gear_sensor = ir(0)

        # NetworkTables

        self.get_gear = False

        self.nt = NetworkTable.getTable('/')
        self.dashboard = NetworkTable.getTable('/SmartDashboard')
        self.dashboard.putString('color', self.ds.getAlliance())

        vision = NetworkTable.getTable('vision')
        vision.putString('selected_camera', 'lower')
        vision.putBoolean('should_process', False)

        # Vision

        self.aiming_light = wpilib.Relay(0)
        wpilib.CameraServer.launch('vision.py:main')

    def autonomous(self):
        """Prepare for autonomous mode"""

        # Call autonomous
        magicbot.MagicRobot.autonomous(self)

    def disabledInit(self):
        """Do once right away when robot is disabled."""
        print('Robot is disabled')

    def disabledPeriodic(self):
        leds.execute(self.get_gear)

    def teleopInit(self):
        self.camera.stop_processing()

    def teleopPeriodic(self):
        """Do periodically while robot is in teleoperated mode."""
        leds.execute(self.get_gear)

        # Get the joystick values and move as much as they say.
        self.drive.forwards_at(-self.driver_stick.getY())
        self.drive.turn_at(helpers.square(self.driver_stick.getX()))

        self.climber.climb_at(self.operator_stick.getY())

        if self.driver_stick.getRawButton(1):
            self.camera.show_high()
        else:
            self.camera.show_low()

        if self.operator_stick.getRawButton(2):
            self.camera.start_processing()
        else:
            self.camera.stop_processing()

        if self.operator_stick.getRawButton(11):
            self.get_gear = True
        else:
            self.get_gear = False

        if self.operator_stick.getRawButton(1):
            self.dumper.dump()
        else:
            self.dumper.reset()

if __name__ == '__main__':
    wpilib.run(MyRobot)
