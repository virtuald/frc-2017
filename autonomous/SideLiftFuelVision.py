"""
This is an autonomous module for driving to the side lift using
vision processing
"""

from components import drive
from components import camera
from components import dumper
from robotpy_ext.autonomous import timed_state, StatefulAutonomous
import wpilib
from magicbot.magic_tunable import tunable


class sideLiftVision(StatefulAutonomous):
    """
    Drives the robot to the side lift using vision processing
    """

    MODE_NAME = "Side Lift Fuel (Vision)"
    DEFAULT = False

    drive = drive.Drive
    camera = camera.Camera
    dumper = dumper.Dumper

    @timed_state(duration=2.0, next_state='forward')
    def dump(self):
        self.dumper.dump()

    @timed_state(duration=1.0, first=True, next_state='slow')
    def forward(self):
        """Drives the majority of the distance to the lift"""
        self.dumper.reset()
        self.drive.forwards_at(0.6)

    @timed_state(duration=4.0, next_state='back_to_boiler')
    def slow(self, initial_call):
        """Drives the remainder of the distance to the lift slowly"""
        if initial_call:
            self.camera.start_processing()
        self.drive.forwards_at(0.35)
        turn_val = self.camera.get_x()
        print(turn_val)
        self.drive.turn_at(turn_val)

    @timed_state(duration=2.0, next_state='back_to_boiler_slow')
    def back_to_boiler(self):
        self.drive.turn_at(-0.05)
        self.drive.forwards_at(-0.6)

    @timed_state(duration=1.0, next_state='dump')
    def back_to_boiler_slow(self):
        self.drive.turn_at(-0.04)
        self.drive.forwards_at(-0.25)

    @timed_state(duration=1.5, next_state='reset')
    def dump(self):
        self.dumper.dump()
        self.drive.forwards_at(-0.3)

    @timed_state(duration=0.3, next_state='dump_2')
    def reset(self):
        self.dumper.reset()
        self.drive.forwards_at(-0.3)

    @timed_state(duration=1.2, next_state='reset_2')
    def dump_2(self):
        self.dumper.dump()
        self.drive.forwards_at(-0.3)

    @timed_state(duration=0.2, next_state='dump_2')
    def reset_2(self):
        self.dumper.reset()
        self.drive.forwards_at(-0.3)
