"""
This is an autonomous module for driving to the center lift
"""

from components import drive
from robotpy_ext.autonomous import timed_state, StatefulAutonomous
import wpilib
from magicbot.magic_tunable import tunable


class MoveForward(StatefulAutonomous):
    """
    Drives the robot to the center lift
    """

    MODE_NAME = "Center Lift"
    DEFAULT = True

    drive = drive.Drive

    @timed_state(duration=1.2, first=True, next_state='slow')
    def fast(self):
        """Drives the majority of the distance to the lift"""
        self.drive.forwards_at(0.4)

    @timed_state(duration=2.5)
    def slow(self):
        """Drives the remainder of the distance to the lift slowly"""
        self.drive.forwards_at(0.25)
