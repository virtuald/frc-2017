"""
This is an autonomous module for driving to the center lift using
vision processing
"""

from components import drive
from components import camera
from robotpy_ext.autonomous import timed_state, StatefulAutonomous
import wpilib
from magicbot.magic_tunable import tunable


class MoveForward(StatefulAutonomous):
    """
    Drives the robot to the center lift using vision processing
    """

    MODE_NAME = "Center Lift (Vision)"
    DEFAULT = False

    drive = drive.Drive
    camera = camera.Camera

    @timed_state(duration=1.2, first=True, next_state='slow')
    def fast(self, initial_call):
        """Drives the majority of the distance to the lift"""
        if initial_call:
            self.camera.start_processing()
        self.drive.forwards_at(0.45)
        turn_val = self.camera.get_x()
        print(turn_val)
        self.drive.turn_at(turn_val)

    @timed_state(duration=5)
    def slow(self):
        """Drives the remainder of the distance to the lift slowly"""
        self.drive.forwards_at(0.30)
        turn_val = self.camera.get_x()
        print(turn_val)
        self.drive.turn_at(turn_val)
