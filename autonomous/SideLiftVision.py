"""
This is an autonomous module for driving to the side lift using
vision processing
"""

from components import drive
from components import camera
from robotpy_ext.autonomous import timed_state, StatefulAutonomous
import wpilib
from magicbot.magic_tunable import tunable


class sideLiftVision(StatefulAutonomous):
    """
    Drives the robot to the side lift using vision processing
    """

    MODE_NAME = "Side Lift (Vision)"
    DEFAULT = False

    drive = drive.Drive
    camera = camera.Camera

    @timed_state(duration=1.2, first=True, next_state='slow')
    def forward(self):
        """Drives the majority of the distance to the lift"""
        self.drive.forwards_at(0.5)

    @timed_state(duration=8.0)
    def slow(self, initial_call):
        """Drives the remainder of the distance to the lift slowly"""
        if initial_call:
            self.camera.start_processing()
        self.drive.forwards_at(0.35)
        turn_val = self.camera.get_x()
        print(turn_val)
        self.drive.turn_at(turn_val)
