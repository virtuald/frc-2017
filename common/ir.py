""" Create IR sensors """
import wpilib

from helpers import *
from networktables.util import ntproperty

# Constants are variables that won't change.
# They belong here


class IR:
    """ Create IR sensors """

    volts_per_inch = ntproperty('/IR/volts_per_inch', 1)
    starting_value = ntproperty('/IR/starting_value', 2000)
    a = ntproperty('/IR/a', 37)
    b = ntproperty('/IR/b', 0.9979)
    k = ntproperty('/IR/k', 3.8)

    def __init__(self, port):
        self.input = wpilib.AnalogInput(port)
        self.input.setAverageBits(500)

    def get(self):
        """ directly returns the average value from the sensor """
        return self.input.getAverageValue()

    def get_inches(self):
        """ returns the number of inches the IR sensor detects
            Please note: The sensor is innacurate if what it is detecting
            is < 2 in. away.
        """

        x = self.get()

        a = self.a
        b = self.b
        k = self.k

        calc_value = (a * (b ** x)) + k

        return int(round(calc_value))
