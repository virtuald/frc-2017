# 1432 2017 FRC Robot Code

**Robot** | [OI](https://gitlab.com/team1432/OI)

[![build status](https://gitlab.com/team1432/frc-2017/badges/master/build.svg)](https://gitlab.com/team1432/frc-2017/commits/master)

This is the Metal Beaver's Python robot code for FRC 2017, SteamWorks. It is powered by [RobotPy](https://robotpy.github.io/).

## File Structure
```
.
├── automations      <- Actions that usually involve more than one component
│   └── __init__.py
├── components       <- Components are physical parts of robot (drivetrain, intake). These interact with WPILib
│   └── __init__.py
├── autonomous       <- Each autonomous mode has a file. These call automations
│   └── __init__.py
├── README.md
└── robot.py         <- Main file, interact with joysticks, call automations.

```

## Contributing

> **Note:** If you do not have write access to the repo, you will need to create a fork first.

1. Create a branch for a feature you want to add.

    ```bash
    $ git checkout -b arm
    ```

2. Create/upload the branch to GitLab

    ```bash
    $ git push -u origin arm
    ```

3. Make your changes and commit them
  
    ```bash
    $ git commit -am 'Add structure to arm component'
    $ git commit -am 'Implement arm lift method'
    ```

4. Upload your changes to GitLab

    ```bash
    $ git push origin arm
    ```

5. Update your branch to be up-to-date with `master`

    ```bash
    $ git fetch
    $ git rebase origin/master
    ```

6. Switch back to `master` branch

    ```bash
    $ git checkout master
    ```
7. Open a pull request on GitLab
8. Ask someone else to review and merge it
