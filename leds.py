import wpilib


class Leds:
    outputs = [
        wpilib.DigitalOutput(channel=0),
        wpilib.DigitalOutput(1)
    ]

    ds = wpilib.DriverStation.getInstance()

    def execute(self, get_gear):
        if get_gear:
            self.outputs[0].set(True)
            self.outputs[1].set(True)
        elif self.ds.getAlliance() == 0:
            self.outputs[0].set(True)
            self.outputs[1].set(False)
        else:
            self.outputs[0].set(False)
            self.outputs[1].set(False)
