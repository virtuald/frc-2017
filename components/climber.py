""" Climber is used to lift the robot to the top of the rope """

import wpilib

from helpers import *
from magicbot.magic_tunable import tunable
from common import ir

# Constants are variables that won't change.
# They belong here


class Climber:
    """
        The sole interaction between the robot and its driving system
        occurs here. Anything that wants to climb the robot must go
        through this class.
    """

    speed = 0
    state = tunable('resting')

    left_climber_motor = wpilib.Talon
    right_climber_motor = wpilib.Talon

    def __init__(self):
        pass

    def on_disabled(self):
        self.state = 'resting'

    # Info functions -- these give information to other methods

    # Verb functions -- these functions do NOT talk to motors directly. This
    # allows multiple callers in the loop to call our functions without
    # conflicts.

    def climb_at(self, speed):
        self.speed = speed

    # Execute function -- This is called automatically at the end of
    # teleopPeriodic / autonomousPeriodic. This directly interacts with
    # WPILib.

    def execute(self):
        """Actually makes the climber move"""

        if -0.09 < self.speed < 0.09:
            self.state = 'resting'
        elif self.speed < 0:
            self.state = 'spinning_reverse'
        else:
            self.state = 'spinning_forwards'

        self.left_climber_motor.set(-self.speed)
        self.right_climber_motor.set(self.speed)
